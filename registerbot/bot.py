"""
Registration bot for Moodle mobile API based clients.
The registration process requires the QR token to be claimed by
a client with the same public IP.
Thus it may be helpful to run this bot in addition to the main PDF bot
to allow registration in a common network while running the more expensive
bot on another machine/network
"""
import os
import telebot
from base64 import b64decode, b64encode
from datetime import datetime, timedelta
from urllib.parse import parse_qs, urlparse
import requests
from  telebot.types import Message
from PIL import Image
from io import BytesIO
from pyzbar import pyzbar
from cryptography.fernet import Fernet

REGISTER_TG_TOKEN = os.environ["REGISTER_TG_TOKEN"]
REGISTER_SHARED_SECRET_B64 = os.environ["REGISTER_SHARED_SECRET_B64"]
TG_BOT_NAME = os.environ["TG_BOT_NAME"]

f = Fernet(REGISTER_SHARED_SECRET_B64)
bot = telebot.TeleBot(REGISTER_TG_TOKEN, num_threads=1)

def token_from_qr_link(qr_link: str) -> tuple[str, int, str, str]:
  """
  Takes in a moodle qr link like
  moodlemobile://https://mycourses.aalto.fi?qrlogin={}&userid={}
  parses it and tries retrieving tokens with it.
  Returns (host, userid, token, pritavetoken) if successful.
  """
  url_data = urlparse(qr_link.removeprefix("moodlemobile://"))
  host = url_data.hostname
  assert host, "Hostname cannot be empty or None."
  query_data = parse_qs(url_data.query)

  res = requests.post(
    f"https://{host}/lib/ajax/service.php",
    headers={
      'user-agent': 'MoodleMobile'
    },
    json=[{
      'index': 0,
      'methodname': 'tool_mobile_get_tokens_for_qr_login',
      'args': {
        'qrloginkey': query_data['qrlogin'][0],
        'userid': query_data['userid'][0]
      }
    }]
  ).json()
  if res[0]['error']:
    raise ValueError("Invalid QR, request to get tokens failed", res[0])

  return host, int(query_data['userid'][0]), res[0]['data']['token'], res[0]['data']['privatetoken']

@bot.message_handler(content_types=['photo'])
def handle_qr(msg: Message):
  file = bot.get_file(msg.photo[-1].file_id)
  img = requests.get(f'https://api.telegram.org/file/bot{REGISTER_TG_TOKEN}/{file.file_path}').content
  pil_img = Image.open(BytesIO(img))
  qrcodes = pyzbar.decode(pil_img, symbols=[pyzbar.ZBarSymbol.QRCODE])
  links = [
    q.data for q in qrcodes if q.data.startswith(b'moodlemobile://')
  ]
  if len(links) != 1:
    bot.send_message(msg.from_user.id, "No valid URLs found from image. Please send a clearer mobile app QR.")
    return
  link = links[0].decode('utf-8')
  try:
    host, userid, token, privtoken = token_from_qr_link(link)
  except ValueError as e:
    print("QR read failed", e)
    bot.send_message(msg.from_user.id, "Logging in with QR failed. Has the QR expired? Please send a fresh one")
    return
  #users[str(msg.from_user.id)] = User(
  #  tg_id=msg.from_user.id,
  #  moodle_id=userid,
  #  moodle_host=host,
  #  moodle_token=token,
  #  moodle_privatetoken=privtoken,
  #  images=()
  #)
  #bot.send_message(msg.from_user.id, "Successfully logged in! Please see upcoming assignments with command /assignments")

  cred_b64 = b64encode(f.encrypt(f"{userid} {host} {token} {privtoken}".encode('utf-8'))).decode('utf-8')
  bot.send_message(
    msg.from_user.id,
    f"Login successful! Please forward following message to @{TG_BOT_NAME}."
  )
  bot.send_message(
    msg.from_user.id,
    f"/register {cred_b64}"
  )

def run():
  bot.infinity_polling()

if __name__ == "__main__":
  run()
