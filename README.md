# Moodle Telegram bot

Allows users to make Moodle PDF submissions from images sent via
Telegram using Moodle WebServices API and mobile app authentication.

## Usage

Fill `.env` file with a bot token. Run with `poe run` after installing
python virtual environment using `poetry shell && poetry install`.
