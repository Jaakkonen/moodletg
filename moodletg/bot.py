from base64 import b64decode
import os
from typing import BinaryIO, Optional
from cryptography.fernet import Fernet
import telebot
import requests
import shelve
from dataclasses import dataclass
import atexit
from telebot.types import Message
from io import BytesIO
from pyzbar import pyzbar
from PIL import Image
from urllib.parse import parse_qs, urlparse
from datetime import datetime, timedelta
import img2pdf


@dataclass
class User:
  tg_id: int
  moodle_id: int
  moodle_host: str
  moodle_token: str
  moodle_privatetoken: str
  images: tuple[str, ...] = ()
  active_assignment: Optional[int] = None

users: shelve.Shelf[User] = shelve.open('users.db')
atexit.register(users.close)

TG_TOKEN = os.environ["TG_TOKEN"]
REGISTER_TG_BOT_NAME = os.environ["REGISTER_TG_BOT_NAME"]
REGISTER_SHARED_SECRET_B64 = os.environ["REGISTER_SHARED_SECRET_B64"]

bot = telebot.TeleBot(TG_TOKEN, num_threads=1)


# def token_from_qr_link(qr_link: str) -> tuple[str, int, str, str]:
#   """
#   Takes in a moodle qr link like
#   moodlemobile://https://mycourses.aalto.fi?qrlogin={}&userid={}
#   parses it and tries retrieving tokens with it.
#   Returns (host, userid, token, pritavetoken) if successful.
#   """
#   url_data = urlparse(qr_link.removeprefix("moodlemobile://"))
#   host = url_data.hostname
#   assert host, "Hostname cannot be empty or None."
#   query_data = parse_qs(url_data.query)

#   res = requests.post(
#     f"https://{host}/lib/ajax/service.php",
#     headers={
#       'user-agent': 'MoodleMobile'
#     },
#     json=[{
#       'index': 0,
#       'methodname': 'tool_mobile_get_tokens_for_qr_login',
#       'args': {
#         'qrloginkey': query_data['qrlogin'][0],
#         'userid': query_data['userid'][0]
#       }
#     }]
#   ).json()
#   if res[0]['error']:
#     raise ValueError("Invalid QR, request to get tokens failed", res[0])

#   return host, int(query_data['userid'][0]), res[0]['data']['token'], res[0]['data']['privatetoken']


def moodle_upload(host, token, **files: BinaryIO):
  res = requests.post(
    f'https://{host}/webservice/upload.php?token={token}',
    files={
      f'file_{index}': (name, file)
      for index, (name, file) in enumerate(files.items(), 1)
    }
  ).json()
  return res


def moodle_ws_req(host, token, method, **args):
    """
    Calls a method using Moodle WebService REST API.
    Can be used for authenticated requests.
    Uses /webservice/rest/server.php
    """
    # Convert lists from args to Moodle webservice list type
    # values=[1,2,3] => values[0]=1&values[1]=2&values[2]=3
    for k, v in list(args.items()):
      if isinstance(v, list):
        del args[k]
        for index, value in enumerate(v):
          args[f'{k}[{index}]'] = value

    res = requests.get(
      f'https://{host}/webservice/rest/server.php',
      params={
        'wstoken': token,
        'wsfunction': method,
        'moodlewsrestformat': 'json',
        **args
      }
    ).json()
    return res


@bot.message_handler(commands=['start'])
def handle_start(msg: Message):
  bot.reply_to(msg,
    "Welcome to MyCourses bot! "
    f"Please start by getting authorization keys from @{REGISTER_TG_BOT_NAME} by "
    "sending it the mobile app QR from Moodle > Username > Profile > View QR Code."
    #"Please start by sending mobile app QR from MyCourses > Username > Profile > View QR Code"
  )
  #bot.send_chat_action(msg.from_user.id, 'upload_photo')
f = Fernet(REGISTER_SHARED_SECRET_B64)
@bot.message_handler(commands=['register']) #, func=lambda msg: msg.forward_from.username == REGISTER_TG_BOT_NAME)
def register(msg: Message):
  # TODO: Add error handling when text is invalid
  userid, host, token, privtoken = f.decrypt(b64decode(msg.text.split(' ')[1])).decode('utf-8').split(' ')
  users[str(msg.from_user.id)] = User(
   tg_id=msg.from_user.id,
   moodle_id=int(userid),
   moodle_host=host,
   moodle_token=token,
   moodle_privatetoken=privtoken,
   images=()
  )
  bot.send_message(msg.from_user.id, "Successfully logged in! Please see upcoming assignments with command /assignments")

# @bot.message_handler(content_types=['photo'], func=lambda msg: str(msg.from_user.id) not in users)
# def handle_qr(msg: Message):
#   file = bot.get_file(msg.photo[-1].file_id)
#   img = requests.get(f'https://api.telegram.org/file/bot{TG_TOKEN}/{file.file_path}').content
#   pil_img = Image.open(BytesIO(img))
#   qrcodes = pyzbar.decode(pil_img, symbols=[pyzbar.ZBarSymbol.QRCODE])
#   links = [
#     q.data for q in qrcodes if q.data.startswith(b'moodlemobile://')
#   ]
#   if len(links) != 1:
#     bot.send_message(msg.from_user.id, "No valid URLs found from image. Please send a clearer mobile app QR.")
#     return
#   link = links[0].decode('utf-8')
#   try:
#     host, userid, token, privtoken = token_from_qr_link(link)
#   except ValueError as e:
#     print("QR read failed", e)
#     bot.send_message(msg.from_user.id, "Logging in with QR failed. Has the QR expired? Please send a fresh one")
#     return
#   users[str(msg.from_user.id)] = User(
#     tg_id=msg.from_user.id,
#     moodle_id=userid,
#     moodle_host=host,
#     moodle_token=token,
#     moodle_privatetoken=privtoken,
#     images=()
#   )
#   bot.send_message(msg.from_user.id, "Successfully logged in! Please see upcoming assignments with command /assignments")


@bot.message_handler(func=lambda msg: str(msg.from_user.id) not in users)
def forward_to_onboarding(msg: Message):
  bot.send_message(msg.from_user.id, "Please login to Moodle. See /start")


# @bot.message_handler(commands=['assignments'])
# def get_assignments(msg: Message):
#   user = users[str(msg.from_user.id)]
#   events = moodle_ws_req(user.moodle_host, user.moodle_token, 'core_calendar_get_calendar_upcoming_view')
#   events_filt = [
#     {
#       'name': f'{ev["course"]["fullname"].split(" - ", 1)[1].split(", ")[0]} - {ev["name"].removesuffix(" is due")}',
#       # NOTE: This is not actual assignment id but instead the module id.
#       'assignment_id': ev['instance'],
#       'course_id': ev['course']['id'],
#       'due': datetime.fromtimestamp(ev['timesort']),
#     } for ev in events['events']
#     if ev['eventtype'] == 'due'
#   ]
#   keyboard = telebot.types.InlineKeyboardMarkup(row_width=1)
#   keyboard.add(*[
#     telebot.types.InlineKeyboardButton(
#       text=ev['name'],
#       callback_data=f"assignment:{ev['assignment_id']}"
#     )
#     for ev in events_filt
#   ])
#   reply = 'You have following assignments coming due:\n\n' + '\n\n'.join([
#     f"<b>{ev['name']}</b>\n{ev['due'].isoformat()}" for ev in events_filt
#   ]) + '\n\n\nPlease click button below to make a submission'
#   bot.send_message(msg.chat.id, reply, reply_markup=keyboard, parse_mode="HTML")
@bot.message_handler(commands=['assignments'])
def get_assignments(msg: Message):
  user = users[str(msg.from_user.id)]
  res = moodle_ws_req(user.moodle_host, user.moodle_token, 'mod_assign_get_assignments')
  # TODO: Add error handling for expired tokens.

  assignments = sorted([
    {
      'name': f'{course["fullname"].split(" - ", 1)[1].split(", ")[0]} - {assignment["name"]}',
      # NOTE: This is not actual assignment id but instead the module id.
      'assignment_id': assignment['id'],
      'course_id': course['id'],
      'due': datetime.fromtimestamp(assignment['duedate']),
    }
    for course in res['courses']
    for assignment in course['assignments']
    if datetime.now() < datetime.fromtimestamp(assignment['duedate']) < datetime.now() + timedelta(weeks=4)
  ], key=lambda x: x['due'])
  # OLD CODE FOR UPCOMING EVENT PARSING
  # events_filt = [
  #   {
  #     'name': f'{ev["course"]["fullname"].split(" - ", 1)[1].split(", ")[0]} - {ev["name"].removesuffix(" is due")}',
  #     # NOTE: This is not actual assignment id but instead the module id.
  #     'assignment_id': ev['instance'],
  #     'course_id': ev['course']['id'],
  #     'due': datetime.fromtimestamp(ev['timesort']),
  #   } for ev in events['events']
  #   if ev['eventtype'] == 'due'
  # ]
  keyboard = telebot.types.InlineKeyboardMarkup(row_width=1)
  keyboard.add(*[
    telebot.types.InlineKeyboardButton(
      text=ev['name'],
      callback_data=f"assignment:{ev['assignment_id']}"
    )
    for ev in assignments
  ])
  reply = 'You have following assignments coming due:\n\n' + '\n\n'.join([
    f"<b>{ev['name']}</b>\n{ev['due'].isoformat()}" for ev in assignments
  ]) + '\n\n\nPlease click button below to make a submission'
  bot.send_message(msg.chat.id, reply, reply_markup=keyboard, parse_mode="HTML")

@bot.callback_query_handler(func=lambda call: call.data.startswith('assignment:'))
def handle_assignment_click(msg: telebot.types.CallbackQuery):
  assignment_id = int(msg.data.removeprefix('assignment:'))

  user = users[str(msg.from_user.id)]
  user.active_assignment = assignment_id
  users[str(msg.from_user.id)] = user

  bot.send_message(msg.from_user.id, 'Assignment selected! Now send photos and submit with /submit')

@bot.message_handler(content_types=['document'], func=lambda msg: users[str(msg.from_user.id)].active_assignment is not None)
def add_photo(msg: Message):
  if msg.document.mime_type not in {'image/png', 'image/jpeg'}:
    bot.reply_to(msg.id, "Unexpected file type. Please only send png and jpeg files")
    return

  user = users[str(msg.from_user.id)]
  file_path = bot.get_file(msg.document.file_id).file_path
  user.images = (*user.images, file_path)
  users[str(msg.from_user.id)] = user
  bot.reply_to(msg, f"Thanks! You have submitted {len(user.images)} pictures thus far for this assignment.")

@bot.message_handler(commands=['clear'])
def clear_photos(msg: Message):
  user = users[str(msg.from_user.id)]
  user.images = ()
  users[str(msg.from_user.id)] = user
  bot.send_message(msg.from_user.id, "Pending images cleared. You now have 0 images to be submitted to the current assignment.")


def layout_fun(imgwidthpx, imgheightpx, ndpi):
    imgwidthpdf = img2pdf.mm_to_pt(210)  # Short size of A4
    imgheightpdf = imgwidthpdf * imgheightpx / imgwidthpx
    pagewidth = imgwidthpdf
    pageheight = imgheightpdf
    return pagewidth, pageheight, imgwidthpdf, imgheightpdf


@bot.message_handler(commands=['submit'])
def submit(msg: Message):
  user = users[str(msg.from_user.id)]
  if user.active_assignment is None:
    bot.send_message(msg.from_user.id, "No active assignment to be submitted to. Please select one from /assignments")
    return
  if not user.images:
    bot.send_message(msg.from_user.id, "No photos pending to be submitted. Please send at least one uncompressed")
    return
  bot.send_message(msg.from_user.id, "Downloading images and converting those to a PDF...")

  images = [
    requests.get(f'https://api.telegram.org/file/bot{TG_TOKEN}/{p}').content
    for p in user.images
  ]
  pdf_io = BytesIO(img2pdf.convert(images, layout_fun=layout_fun))
  pdf_io.name = "submission.pdf"

  bot.send_chat_action(msg.from_user.id, 'upload_photo')
  moodle_file = moodle_upload(user.moodle_host, user.moodle_token, **{
    'submission.pdf': pdf_io
  })
  itemid = moodle_file[0]['itemid']

  res = moodle_ws_req(user.moodle_host, user.moodle_token, 'mod_assign_save_submission', **{
    'assignmentid': str(user.active_assignment),
    'plugindata[files_filemanager]': itemid
  })
  bot.send_message(msg.from_user.id, "Submitted!")
  #bot.send_document(msg.from_user.id, pdf_io, )

  user.images = ()
  user.active_assignment = None
  users[str(msg.from_user.id)] = user


def run():
  bot.infinity_polling()
